﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorColorChange : MonoBehaviour
{
    public string materialName;
    public MeshRenderer mesh;
    private Button btn;
    void Start()
    {
        mesh = GameObject.FindGameObjectWithTag("Door").GetComponent<MeshRenderer>();
        //btn = GetComponent<Button>();
        //btn.onClick.AddListener(ChangeColor);
    }
    public void ChangeColor(){
        UnityEngine.Debug.Log("in");
        mesh.material = Resources.Load<Material>(materialName);
    }
    void Update(){
        if(Input.GetKeyDown(KeyCode.Space)){
            ChangeColor();
        }
    }
    
}
