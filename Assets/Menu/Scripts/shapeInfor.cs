﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class shapeInfor : MonoBehaviour
{
    public GameObject TargetObj;
    private Button btn;

    private Text text;
    public string name;

    string path = "ModelToLoad/";

    void Start(){
        btn = GetComponent<Button>();
        btn.onClick.AddListener(OnButtonClick);
        TargetObj = GameObject.FindGameObjectWithTag("Player");
        text = GetComponentInChildren<Text>();
        text.text = name;
    }
    void OnButtonClick(){
        TargetObj.GetComponent<MeshFilter>().mesh = Resources.Load<Mesh>("ModelToLoad/"+name);
    }
}
