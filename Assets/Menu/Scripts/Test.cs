﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Xml;
using System.Xml.Serialization;
//using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;

public class Test : MonoBehaviour
{
    public GameObject TargetObj;
    public GameObject shapeButton;

    public GameObject ButtonPanel;
    private Button btn;
    List<Shape> shapes = new List<Shape>();
    
    // Start is called before the first frame update
    void Start()
    {
        shapes = GetPantRecordset(Application.dataPath + "/Resources/test.xml");
        foreach (Shape item in shapes)
        {
            // UnityEngine.Debug.Log("dvsvf");
            // GameObject newObj = Instannitaewt();
            // b=newObj.GetComponent<objInfo>.Name = item.anme;

            GameObject button = Instantiate(shapeButton, transform, false);
            button.GetComponent<shapeInfor>().name = item.Name;
            button.transform.SetParent(ButtonPanel.transform);
            //print(item.Name);
        }
        Shape find = new Shape();
        foreach(Shape item in shapes){
            if(item.Name == "Hinh vuong"){
                find = item;
            }
            if(item.Name == "Hinh cau"){
                find = item;
            }
        }
        Debug.Log(find.Name+" "+find.Material);
    }
    void Update(){
        //if(Input.GetKeyDown(KeyCode.Space)){
        //    TargetObj.GetComponent<MeshFilter>().mesh = Resources.Load<GameObject>("ModelToLoad/Hinh vuong").GetComponent<MeshFilter>().sharedMesh;
        //}
    }
    public List<Shape> GetPantRecordset(string path)
    {
        
        //string path = Application.dataPath + "/Resources/" + pantTable;

        List<Shape> toReturn = new List<Shape>();

        if (File.Exists(path))
        {

            XmlSerializer reader = new XmlSerializer(typeof(List<Shape>));

            StreamReader file = new StreamReader(path);

            toReturn = (List<Shape>)reader.Deserialize(file);

            file.Close();
        }

        return toReturn;
    }
}
