﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowInformation : MonoBehaviour
{
    public Text Name;
    public Text Age;
    public Text Gender;
    public Text Detail;
 
    public List<Text> Texts;
 
    public void OnButtonClick()
    {
        this.Name.text = "Nguyễn Đăng Hà";
        this.Age.text = "21";
        this.Gender.text = "Male";
        this.Detail.text = "Ho, đau nhức xương khớp";
    }
}
