﻿using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class autoBodyController : MonoBehaviour
{
    public Animator bodyAnimation;
    
    static bool isAction = false;
    static int count = 0;
    static int actionNumber = 0;
    string filePath = @"F:\IT\CSharpOut\OutputTesting\ActionInput.txt";
    List<string> testInput = new List<string>();

    // Start is called before the first frame update
    void Start()
    {
        bodyAnimation = GetComponent<Animator>(); 
        //Execute();
        StartCoroutine(ExecuteAfterTime());
    }

    public IEnumerator ExecuteAfterTime()
    {
        WaitForSeconds wait = new WaitForSeconds(5);
        
        for (int i = 0; i < 10; i++) 
        {
            yield return wait;
            Execute();
        }
        
    } 

    void Execute() 
    {
        if (File.Exists(filePath))
        {
            testInput = File.ReadAllLines(filePath).ToList();

            foreach (String line in testInput)
            {
                count += 1;
            }
            
            if (count != 0)
            {
                actionNumber = Convert.ToInt32(testInput[count-1]);
                if(actionNumber == 1) 
                {
                    bodyAnimation.Play("Sit Down");
                    isAction = true;
                }

                if(actionNumber == 2) 
                {
                    bodyAnimation.Play("Ready Hands");
                    isAction = false;
                }

                if(actionNumber == 3) 
                {
                    bodyAnimation.Play("Left Hand Raise");
                    isAction = true;
                }

                if(actionNumber == 4) 
                {
                    bodyAnimation.Play("Right Hand Raise");
                    isAction = true;
                }

                if(actionNumber == 5) 
                {
                    bodyAnimation.Play("Stand Up");
                    isAction = false;
                }

                if(actionNumber == 6) 
                {
                    bodyAnimation.Play("Left Knee Raise");
                    isAction = true;
                }

                if(actionNumber == 7) 
                {
                    bodyAnimation.Play("Right Knee Raise");
                    isAction = true;
                }

                if(actionNumber == 8) 
                {
                    bodyAnimation.Play("Left Leg Raise");
                    isAction = false;
                }

                if(actionNumber == 9) 
                {
                    bodyAnimation.Play("Right Leg Raise");
                    isAction = true;
                }

            }
            //Clear list
            //testInput.Clear();
            count = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {   
        //Execute();
    }
}