﻿using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegAnimationController : MonoBehaviour
{
    public Animator legAni;
    
    static bool isAction = false;

    /* float time;
    float timeDelay; */

    string filePath = @"F:\IT\C#\ActionInput.txt";
    List<string> textInput = new List<string>();

    //System.Timers.Timer t = new System.Timers.Timer(2000);

    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);

        // Code to execute after the delay
    } 

    // Start is called before the first frame update
    void Start()
    {
        legAni = GetComponent<Animator>(); 
        Execute();
    }

    void Execute() 
    {
        if(Input.GetKeyDown("1") && !isAction) {
            legAni.Play("Raise Left Knee");
            isAction = true;
            //textInput.Remove();
        }

        if(Input.GetKeyDown("2") && isAction) {
            legAni.Play("Drop Left Knee");
            isAction = false;
        }

        if(Input.GetKeyDown("3") && !isAction) {
            legAni.Play("Raise Right Knee");
            isAction = true;
        }

        if(Input.GetKeyDown("4") && isAction) {
            legAni.Play("Drop Right Knee");
            isAction = false;
        }

        if(Input.GetKeyDown("5") && !isAction) {
            legAni.Play("Raise Left Leg");
            isAction = true;
        }

        if(Input.GetKeyDown("6") && isAction) {
            legAni.Play("Drop Left Leg");
            isAction = false;
        }

        if(Input.GetKeyDown("7") && !isAction) {
            legAni.Play("Raise Right Leg");
            isAction = true;
        }

        if(Input.GetKeyDown("8") && isAction) {
            legAni.Play("Drop Right Leg");
            isAction = false;
        }
    }

    // Update is called once per frame
    void Update()
    {   
        
        Execute();
    }
}
